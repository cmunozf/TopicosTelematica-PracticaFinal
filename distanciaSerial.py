#!/usr/bin/env python
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from operator import itemgetter
import time

def find_similar(tfidf_matrix, len,namedocs):
    cosine_similarities = linear_kernel(tfidf_matrix[0:len], tfidf_matrix)
    matrix = []
    l = []
    i = 0
    for array in cosine_similarities:
        for index, object in enumerate(array):
            l.append([namedocs[index],"{0:.3}".format(object)])
        i+=1
        l1 = sorted(l, key=itemgetter(1),reverse=True)
        matrix.append(l1[:5])
        l = []
    return matrix


def get_document_filenames():
    rootDir = root+"/dataset"
    fileSet = []

    for dir_, _, files in os.walk(rootDir):
        for fileName in files:
            relDir = os.path.relpath(dir_, rootDir)
            relFile = os.path.join(relDir, fileName)
            fileSet.append(rootDir+'/'+relFile)
    return fileSet


def create_vectorizer():
    fVocabuario = open(root+"/vocabulario2.txt","r")
    vocabulario = fVocabuario.read().split(" ")
    fVocabuario.close()
    return TfidfVectorizer(input='filename',
                           decode_error='replace',
                           ngram_range=(1,1),
                           vocabulary=vocabulario)

#Se guarda el tiempo cuando inicia
start_time = time.time()
#Se obtiene el directorio root
root = os.getcwd()

#Se crea un vector TfidfVectorizer
vectorizer = create_vectorizer()

#Obtener una lista de la direccion de los documentos
docs = get_document_filenames()

#Se crea el vectorTfIdf que es el de frecuencias de palabras en los documentos
#Se envian todos los documentos y este se evalua con el vector creado anteriormente
#que es el que tiene el vocabulario
tfidf_result = vectorizer.fit_transform(docs)

#Se obtienen los nombres de los documentos
namedocs = {}
i = 0
for doc in docs:
    namedocs[i] = os.path.basename(doc)
    i+=1

#Se envia el vector tfidf para buscar la similitud del coseno
matrix = find_similar(tfidf_result,len(docs),namedocs)

#Se imprime el tiempo transcurrido en segundos
print("--- %s seconds ---" % (time.time() - start_time))

#Se guarda la matrix de similitud en un archivo con el tiempo de ejecucion
f = open('output/outSerial.txt', 'w')
print >> f, time.time() - start_time
print >> f , ""
print >> f, (matrix)
f.close()



