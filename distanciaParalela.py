#!/usr/bin/env python
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from operator import itemgetter
import time
import numpy as np
from mpi4py import MPI

def find_similar(tfidf_matrix, len,namedocs):
    cosine_similarities = linear_kernel(tfidf_matrix[0:len], tfidf_matrix)
    matrix = []
    l = []
    i = 0
    for array in cosine_similarities:
        #a = sorted(array,key=float,reverse=True)
        for index, object in enumerate(array):
            l.append([namedocs[index],"{0:.3}".format(object)])
        i+=1
        l1 = sorted(l, key=itemgetter(1),reverse=True)
        matrix.append(l1[:5])
        l = []
    return matrix


def get_document_filenames():
    rootDir = root+"/dataset"
    fileSet = []

    for dir_, _, files in os.walk(rootDir):
        for fileName in files:
            relDir = os.path.relpath(dir_, rootDir)
            relFile = os.path.join(relDir, fileName)
            fileSet.append(rootDir+'/'+relFile)
    return fileSet


def create_vectorizer():
    fVocabuario = open(root+"/vocabulario2.txt","r")
    vocabulario = fVocabuario.read().split(' ')
    fVocabuario.close()
    return TfidfVectorizer(input='filename',
                           decode_error='replace',
                           ngram_range=(1,1),
                           vocabulary=vocabulario)

#Se guarda el tiempo cuando inicia
start_time = time.time()

comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size
name = MPI.Get_processor_name()

#Se obtiene el directorio root
root = os.getcwd()

#Obtener una lista de la direccion de los documentos
docs = get_document_filenames()
print(len(docs))
#Se divide el numero de documentos en partes iguales para los core
Ndocs = int(round(len(docs)/size))


#Se obtienen los nombres de los documentos
namedocs = {}
i = 0
for doc in docs:
    namedocs[i] = os.path.basename(doc)
    i+=1

#Cada uno crea un vector TfidfVectorizer
vectorizer = create_vectorizer()

#para los que tienen la primera parte se asigna los documentos
#Pero como se redondeo la division de los grupos de documentos
#Al final puede que falten algunos documentos
if rank<(size-1):
    #Se reparte a cada core los documentos necesarios
    docsPart = docs[((rank*Ndocs)):(((rank+1)*Ndocs))]
else:
    #Si es el ultimo core se le da su grupo HASTA EL FINAL de todos los documentos
    docsPart = docs[(rank*Ndocs):]

#Se crea el vectorTfIdf que es el de frecuencias de palabras en los documentos
#Se envian todos los documentos y este se evalua con el vector creado anteriormente
#que es el que tiene el vocabulario

tfidf_resultPart = vectorizer.fit_transform(docsPart)

#Cada uno amplia la lista para poder agrupar todos los vectores
#matrixPart = csr_matrix(tfidf_resultPart).todense()


#Como el core 1 es el que va a ejecutar lo siguiente
#Todos los demas le envian sus resultados
if rank!=1:
    comm.send(tfidf_resultPart,1)
    matrixPart = []

if rank == 1:
    #Se agrupan los vectores.dense()
    for i in range(0,size):
        matrixPart1 = []
        if i == 0:
            matrixPart1 = comm.recv(source=0).todense()
            #Se asigna a matrixFinal lo que encontro el core 0
            matrixFinal = matrixPart1
        elif i == 1:
            #se concatena el resultado del core 0 y de este core en
            #el que se ejecuta toda esta parte
            matrixPart1 = tfidf_resultPart.todense()
            tfidf_resultPart = []
            matrixFinal = np.concatenate([matrixFinal,matrixPart1],axis=0)
        else:
            matrixPart1 = comm.recv(source=i).todense()
            #Si es cualquier otro core tan solo se concatena con lo que se recibe
            matrixFinal = np.concatenate([matrixFinal,matrixPart1],axis=0)
        matrixPart1 = []



    #Cuando concatena todos los documentos
    #Se envia el vector tfidf para buscar la similitud del coseno
    matrixSimilitud = find_similar(matrixFinal,len(docs),namedocs)

    #Se imprime el tiempo transcurrido en segundos
    print(rank,"--- %s seconds ---" % (time.time() - start_time))

    #Se guarda la matrix de similitud en un archivo con el tiempo de ejecucion
    f = open('output/outParalelo.txt', 'w')
    print >> f, time.time() - start_time
    print >> f , ""
    print >> f, (matrixSimilitud)
    f.close()

print(rank,"--- %s seconds ---" % (time.time() - start_time))







